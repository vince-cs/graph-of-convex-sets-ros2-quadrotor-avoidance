#pragma once


// #include <rclcpp/rclcpp.hpp>
// #include <px4_ros2/navigation/experimental/local_position_measurement_interface.hpp>

// //boiler plate tempalte
// class MyMode : public px4_ros2::ModeBase // [1]
// {
// public:
//   explicit MyMode(rclcpp::Node & node)
//   : ModeBase(node, Settings{"My Mode"}) // [2]
//   {
//     // [3]
//     _manual_control_input = std::make_shared<px4_ros2::ManualControlInput>(*this);
//     _rates_setpoint = std::make_shared<px4_ros2::RatesSetpointType>(*this);
//   }

//   void onActivate() override
//   {
//     // Called whenever our mode gets selected
//   }

//   void onDeactivate() override
//   {
//     // Called when our mode gets deactivated
//   }

//   void updateSetpoint(const rclcpp::Duration & dt) override
//   {
//     // [4]
//     const Eigen::Vector3f thrust_sp{0.F, 0.F, -_manual_control_input->throttle()};
//     const Eigen::Vector3f rates_sp{
//       _manual_control_input->roll() * 150.F * M_PI / 180.F,
//       -_manual_control_input->pitch() * 150.F * M_PI / 180.F,
//       _manual_control_input->yaw() * 100.F * M_PI / 180.F
//     };
//     _rates_setpoint->update(rates_sp, thrust_sp);
//   }

// private:
//   std::shared_ptr<px4_ros2::ManualControlInput> _manual_control_input;
//   std::shared_ptr<px4_ros2::RatesSetpointType> _rates_setpoint;
// };